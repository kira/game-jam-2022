﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aphid : MonoBehaviour
{
    public GameObject aphidPrefab;
    public float aphidSpawnCD = 2.0f;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(aphidSpawner());
    }

    private void spawnAphid()
    {
        GameObject a = Instantiate(aphidPrefab) as GameObject;
        a.transform.position = new Vector3(Random.Range(-10, 10), 0.5f, Random.Range(-10, 10));
    }

    IEnumerator aphidSpawner()
    {
        while (true) {
            yield return new WaitForSeconds(aphidSpawnCD);
            spawnAphid();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
