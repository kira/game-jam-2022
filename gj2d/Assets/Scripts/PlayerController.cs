using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public string inputNameHorizontal;
    public string inputNameVertical;

    public float moveSpeed;
    public float rotationSpeed;
    
    public Rigidbody2D rb;

    public Vector2 movement;
    Vector2 rotation;
 

    void Update()
    {
        movement.x = 0;
        movement.y = Input.GetAxisRaw(inputNameVertical);
        
        rotation.x = Input.GetAxisRaw(inputNameHorizontal);
        rotation.y = 0;
    }

    public void FixedUpdate()
    {
        //if (movement.y > 0)
        //{
            rb.velocity = transform.up * movement.y * moveSpeed* Time.fixedDeltaTime;
            //transform.Translate(Vector3.forward * moveSpeed * Time.fixedDeltaTime);
            
            //rb.for
            //rb.MovePosition(rb.position + (new Vector2(rb.transform.forward.x, rb.transform.forward.y) * movement.y) * moveSpeed * Time.fixedDeltaTime);
        //}

        rb.MoveRotation(rb.rotation + -rotation.x * rotationSpeed * Time.fixedDeltaTime);
    }
}
