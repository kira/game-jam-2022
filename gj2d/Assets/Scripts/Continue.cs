﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Continue : MonoBehaviour
{
    public GameObject canvas;

    public void Start()
    {
        Time.timeScale = 0f;
    }
    public void ContinueGame()
    {
        canvas.SetActive(false);
        Time.timeScale = 1f;
    }
}
