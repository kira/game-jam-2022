﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sugar : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.gameObject.layer == 11)
        {
            Destroy(this.gameObject);
            UIManager.instance.AddPoints();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.gameObject.layer == 11)
        {
            Destroy(this.gameObject);
            UIManager.instance.AddPoints();
        }
        
        if (other.transform.gameObject.layer == 8)
        {
            Destroy(this.gameObject);
            UIManager.instance.AddPoints();
        }
    }
}
