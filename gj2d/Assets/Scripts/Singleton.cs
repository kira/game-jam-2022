﻿using UnityEngine;

public abstract class Singleton<T> : DontDestroyOnLoad where T : Singleton<T>
#if UNITY_EDITOR
    , new()
#endif
{
    private static T _instance;

    public static T instance
    {
        get
        {
            if (_instance != null && _instance.enabled == false)
            {
                _instance.enabled = true;
            }

            return _instance;
        }
        private set => _instance = value;
    }

    protected override void Awake()
    {
        instance = InstanceCheck(instance) as T;
#if UNITY_EDITOR
        Application.quitting += ResetStaticValues;
#endif
    }

    protected Singleton<T> InstanceCheck(Singleton<T> instanceCheck)
    {
        if (instanceCheck != null && instanceCheck != this)
        {
            DestroyImmediate(instanceCheck.gameObject);
        }

        return this;
    }

    [ExecuteInEditMode]
    public void CreateEditorInstance()
    {
        instance = InstanceCheck(instance) as T;
    }

#if UNITY_EDITOR
    /// <summary>
    /// Manually create the instance.
    /// </summary>
    public static void CreateDefaultInstance()
    {
        instance = new T();
    }
#endif

    protected virtual void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }

#if UNITY_EDITOR
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    static void ResetStaticValues()
    {
        if (instance != null)
        {
            instance = null;
        }

        Application.quitting -= ResetStaticValues;
    }
#endif
}