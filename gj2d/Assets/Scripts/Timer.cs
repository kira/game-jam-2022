﻿using UnityEngine;
using System.Collections;

    public class Timer
    {
        bool _isActive = false;
        public float _time = 0;

        public bool IsOnDelay(float waitTime, float deltaTime, bool isAutoReset = true)
        {
            if (waitTime <= 0.0f)
            {
                _isActive = false;
                return false;
            }
            else
            {
                if (_isActive == false)
                {
                    if (isAutoReset)
                    {
                        _isActive = true;
                        _time = 0;
                    }
                    else
                        return false;
                }
                else
                {
                    if (_time < waitTime)
                    {
                        _time += deltaTime;
                    }
                    else
                    {
                        _isActive = false;
                        return false;
                    }
                }
            }

            return true;
        }

        public float GetTime()
        {
            return _time;
        }

        public void Reset(bool isActive = true)
        {
            _isActive = isActive;
            _time = 0; 
        }
    }