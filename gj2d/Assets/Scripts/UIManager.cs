﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    public Text scoreText;

    public int score;

    public Text ladybugScoreText;

    public int ladybugScore;
    
    public Text cooldownText;
    
    public void AddPoints()
    {
        score+=2;

        scoreText.text = score.ToString();
    }
    
    public void AddLadybugPoints()
    {
        ladybugScore++;

        ladybugScoreText.text = ladybugScore.ToString();
    }
}
