﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class Bug : MonoBehaviour
{
    private float dirX;

    private float dirY;

    public float speed;
    
    Vector2 movement;

    public Rigidbody2D rb;

    private bool isMoving = true;
    
    Timer timer = new Timer();
    private float waitTime;

    public GameObject bugPrefab;
    public GameObject sugarPrefab;
    
    // Start is called before the first frame update
    void Start()
    {
        movement.x = Random.Range(-1, 1);
        movement.y = Random.Range(-1, 1);

        waitTime = Random.Range(30, 50);
    }

    private void Update()
    {
        if (timer.IsOnDelay(waitTime, Time.fixedDeltaTime) == false)
        {
            isMoving = !isMoving;

            if (isMoving)
            {
                movement.x = Random.Range(-1, 1);
                movement.y = Random.Range(-1, 1);
            }
            else
            {
                GameObject.Instantiate(bugPrefab,this.transform.position, this.transform.rotation);
                GameObject.Instantiate(sugarPrefab,this.transform.position, this.transform.rotation);
            }
        }
    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            rb.MovePosition(rb.position + movement * speed * Time.fixedDeltaTime);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.gameObject.layer == 10)
        {
            Destroy(this.gameObject);
            
            UIManager.instance.AddLadybugPoints();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.gameObject.layer == 10)
        {
            Destroy(this.gameObject);
            
            UIManager.instance.AddLadybugPoints();
        }

        if (other.transform.gameObject.layer == 13)
        {
            Destroy(this.gameObject);
        }
    }
}
