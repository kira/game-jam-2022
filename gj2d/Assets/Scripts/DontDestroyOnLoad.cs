﻿using UnityEngine;
using System.Collections;

public class DontDestroyOnLoad : MonoBehaviour
{
    protected virtual void Awake()
    {
        Transform parent = transform.parent;
        while (parent)
        {
            if (GetComponent<DontDestroyOnLoad>())
                return;//break Awake
            parent = parent.parent;
        }
        DontDestroyOnLoad(gameObject);
    }
}
