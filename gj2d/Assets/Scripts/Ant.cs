﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ant : MonoBehaviour
{
    public Transform current;

    private bool isBugGrab = false;
    public Transform grabed;
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        /*
        if (other.transform.gameObject.layer == 12)
        {
            UIManager.instance.AddPoints();
            Destroy(other.gameObject);
        }
        */

        if (other.transform.gameObject.layer == 9)
        {
            current = other.transform;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform == current)
        {
            current = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.gameObject.layer == 12 || other.transform.gameObject.layer == 9)
        {
            current = other.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform == current)
        {
            current = null;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (current != null && grabed == null)
            {
                if (current.transform.gameObject.layer == 12)
                {
                    UIManager.instance.AddPoints();
                    Destroy(this.current.gameObject);
                }
                else
                {
                    current.transform.parent = this.transform;
                    grabed = current;
                }
            }
            else if (grabed != null)
            {
                grabed.parent = null;

                grabed = null;
                isBugGrab = false;
            }
        }
    }
}
