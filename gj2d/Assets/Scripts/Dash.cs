﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour
{
    public float activeMoveSpeed;


    public float dashSpeed = 500;
    public PlayerController playerController;

    Timer timer = new Timer();

    private bool isDashEnabled = true;
    
    // Start is called before the first frame update
    void Start()
    {
        activeMoveSpeed = playerController.moveSpeed;  
    }

    // Update is called once per frame
    void Update()
    {
        playerController.rb.velocity = playerController.movement * activeMoveSpeed;

        if (isDashEnabled == false)
        {
            if (timer.IsOnDelay(5, Time.deltaTime) == false)
            {
                isDashEnabled = true;
                UIManager.instance.cooldownText.text = "0";
            }
            else
            {
                UIManager.instance.cooldownText.text = (5- timer._time).ToString("0.0");
            }
        }

        if (Input.GetKeyDown(KeyCode.P) && isDashEnabled)
        {
            playerController.rb.AddForce(transform.up * dashSpeed * Time.deltaTime);

            isDashEnabled = false;
        }

        /*
        if (dashCounter > 0)
        {
            dashCounter -= Time.deltaTime;

            if (dashCounter <= 0)
            {
                activeMoveSpeed = playerController.moveSpeed;
                dashCoolCounter = dashCooldown;
            }
        }

        if (dashCoolCounter > 0)
        {
            dashCoolCounter -= Time.deltaTime;
        }*/
    }
}
